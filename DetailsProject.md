# Details of the project

## Keepcoding project

Primero he creado el archivo DeepLearning_data.pynb donde únicamente he implementado y testeado un modelo con keras para datos numéricos y categóricos.

He hecho bastantes pruebas para optimizar el modelo.

|Layers|Neurons|Learning rate|Activation|Optimization|Epochs|Bach_size|Loss|Mean|Std| Description|Improve	
|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
|5|32,16,8,4,1|0,001|Relu|Adam|200|25|mean_absolute_percentage_error|24,66|22,62|First version|	
|5|32,16,8,4,1|0,001|Relu|Adam|300|25|mean_absolute_percentage_error|24,75|24,33|Increase epoch|NO
|5|32,16,8,4,1|0,001|Relu|Adam|150|25|mean_absolute_percentage_error|24,48|23,34|Decrease epoch|NO
|4|16,8,4,1|0,001|Relu|Adam|200|25|mean_absolute_percentage_error|25,1| 22,65|Decrease layers|NO
|6|64,32,16,8,4,1|0,001|Relu|Adam|200|25|mean_absolute_percentage_error|25,39|26,13|Increase layer|NO
|5|32,16,8,4,1|0,01|Relu|Adam|200|25|mean_absolute_percentage_error|30.01|30.42|Increase learnign rate|NO

Parece que la mejor configurración para nuestro modelo según los datos de la tabla, era la primera que se configuró.

Una vez que ya tenía este modelo opimizado dentro de uestras posibilidades, es hora de añadir el modelo para convolucional para el análisis de datos,

Para ello cree el archivo DeepLearning_with_images . Incluye procesamiento de datos e imágenes.

Lo más destacable aquí, es que he limitado el tamaño del dataframe de entrenamiento a 2000 datos para poder trabajar bien. Ya que sino requería de mucho tiempo el procesamiento de toda la información.