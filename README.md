# Práctica de Deep Learning

## Keepcoding project

Este proyecto tiene como objetivo crear un modelo que nos permita calcular el precio de una vivienda de airbnb con la mayor exactitud posible. Para ello emplearemos un conjunto de datos e imágenes para entrenar y validar nuestro modelo predictivo.

Este proyecto consta de 3 partes:

 - Descarga y limpieza del dataset de airbnb
 - Creación de modelo de entrenamiento de imágenes y datos numéricos y categóricos.
 - Entrenamiento del modelo creado.

## Descarga, análisis y limpieza de datos
En este caso vamos a emplear el siguiente dataset de airbnb que podemos descargar en el siguiente enlace: [airbnb data](https://public.opendatasoft.com/explore/dataset/airbnb-listings/export/?disjunctive.host_verifications&disjunctive.amenities&disjunctive.features&q=Madrid&dataChart=eyJxdWVyaWVzIjpbeyJjaGFydHMiOlt7InR5cGUiOiJjb2x1bW4iLCJmdW5jIjoiQ09VTlQiLCJ5QXhpcyI6Imhvc3RfbGlzdGluZ3NfY291bnQiLCJzY2llbnRpZmljRGlzcGxheSI6dHJ1ZSwiY29sb3IiOiJyYW5nZS1jdXN0b20ifV0sInhBeGlzIjoiY2l0eSIsIm1heHBvaW50cyI6IiIsInRpbWVzY2FsZSI6IiIsInNvcnQiOiIiLCJzZXJpZXNCcmVha2Rvd24iOiJyb29tX3R5cGUiLCJjb25maWciOnsiZGF0YXNldCI6ImFpcmJuYi1saXN0aW5ncyIsIm9wdGlvbnMiOnsiZGlzanVuY3RpdmUuaG9zdF92ZXJpZmljYXRpb25zIjp0cnVlLCJkaXNqdW5jdGl2ZS5hbWVuaXRpZXMiOnRydWUsImRpc2p1bmN0aXZlLmZlYXR1cmVzIjp0cnVlfX19XSwidGltZXNjYWxlIjoiIiwiZGlzcGxheUxlZ2VuZCI6dHJ1ZSwiYWxpZ25Nb250aCI6dHJ1ZX0%3D&location=16,41.38377,2.15774&basemap=jawg.streets)

Una vez descargados todos los datos, Lo primero es separar el dataset en dos partes: train y test.  Para separar el dataset en dos partes de una forma rápida y eficiente, emplearemos la función [train_test_split](https://scikit-learn.org/stable/modules/generated/sklearn.model_selection.train_test_split.html). 

El análisis y limpieza de datos previo a la creación del modelo lo haremos sobre el dataset de train. 

#### Análisis del dataset

Analizamos todos los campos de información que tenemos. 

 En este caso, lo primero que hacemos es filtrar por la ciudad para quedarnos únicamente con datos de Madrid.

Los pasos recomendables a la hora de analizar un dataset, son los siguientes:

 - Descartar aquellos campos que veamos que no van a ser relevantes a la hora de crear el modelo. Pueden ser campos que aparecen duplicados, o  campos que tienen alta correlación con otros campos, o campos que aportan información escrita  irrelevante.
 
 - Analizar que campos cuentan con un alto porcentaje de [valores nulos o no válidos](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.isnull.html). [Eliminamos](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.drop.html) aquellos campos que tengan un porcenaje de valores nulos  superior al 25%. 
 - Creamos una matriz de correlación y vemos que datos están muy correlaciones entre si. De esta forma podemos eliminar columnas que representan practicamente la misma información.
 - Eliminar los outliers. Para ello podemos emplear [histogramas](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.plot.hist.html) que nos muestran gráficamente si tenemos datos aislados o puntuales que pueden entorpecer el entrenamiento posterior. Eliminamos los posibles outliers. 
 - Analizar en que campos tiene sentido [sustituir los valores nulos con la media o la moda](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.fillna.html).
 - Analizar si podemos procesar la información de algún campo con información que no es útil para convertirla en información útil. En este caso se procesan fechas para ver cuantos años lleva en airbnb una vivienda o con cuantos servicios cuenta una vivienda.
 - [Eliminar](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.dropna.html) los posibles valores nulos que puedan quedar en el dataframe. Si no hemos podido sustituirlos por la moda o la media.
 - Categorizar datos no numéricos. Esto es muy importante, ya que no podremos emplear esa información para  entrenar nuestro modelo si no categorizamos previamente dicha información y le damos valores numéricos  que la identifiquen.

#### Procesamiento de imágenes
Como se ha comentado anteriormente, queremos crear un modelo que trabaje con datos e imágenes conjuntamente.

Para ello es importante procesar todas las imágenes que tenemos en el dataset. En este caso tenemos acceso a la url de cada imagen. Por lo que tenemos que descargar la imagen  procesarla para darle un formato que nuestro modelo pueda entender y utilizar.

Para ello seguimos los siguientes pasos:
 - Cogemos la columna del dataframe que contiene las url de todas las imágenes y las [descargamos](https://scikit-image.org/docs/dev/api/skimage.io.html).
 
 - Puede que algunas url no funcionen, por ello tenemos que quedarnos con aquellas filas que tengan url válidas.
 - Le [damos formato RGB](https://www.geeksforgeeks.org/python-opencv-cv2-imshow-method/) a aquellas imágenes que no lo tengan y llevamos a cabo un [resize](https://www.tutorialkart.com/opencv/python/opencv-python-resize-image/) para darles a todas el mismo formato.
 - Ahora tenemos las imágenes con el formato correcto. La función io.imread convierte la  imagen en una matriz de formato (64,64,3).
 - Guardamos las imágenes como un numpy array y eliminamos del dataframe la columna que contiene las url de las imágenes ya que procesaremos ambas cosas separadas.
 

#### Limpieza del dataset y procesamiento de imágenes en test.

Una vez hemos limpiado el dataset de train, aplicamos todos esos cambios y el procesamiento de imágenes que hemos hecho en train al dataset de test.

Esto se hace para evitar que cuando nos llegue información nueva a nuestro modelo de predicción, no aparezcan valores que nuestro modelo no reconoce.

##  Creación de modelo de entrenamiento

En este caso necesitamos un [modelo que sea capaz de trabajar tanto con datos numéricos y categorícos como con imágenes](https://www.pyimagesearch.com/2019/02/04/keras-multiple-inputs-and-mixed-data/). Para ello vamos a utilizar [Keras](https://keras.io/api/losses/), ya que simplifica mucho todo el proceso.

Para crear un modelo predictivo que trabaje con datos y con imágenes conjuntamente, lo primero que haremos será trabajar con los datos por un lado, y con las imágenes por otro y finalmente acabaremos fusionando  ambos  modelos para crear un único modelo.

#### Creación de X_train, X_test, y_train, y_test y normalización de los datos. 

En el caso de los datos numéricos tenemos que seguir los siguientes pasos:
 - Tenemos que separar tanto en train como en test los datos que vamos a utilizar para entrenar el modelo por un lado y por otro lado la variable, el campo cuyo valor  queremos predecir.
 
 - Por lo tanto ahora tenemos X_train, X_test, y_train e y_test. Donde "X" representa el conjunto de dato de entrenamiento e "y" el conjunto de valores de la variable que queremos predecir.
 - Aplicamos  una normalización sobre los datos para que nuestro modelo funcione correctamente. En el caso de X_train y X_test podemos emplear [MinMaxScaler](https://scikit-learn.org/stable/modules/generated/sklearn.preprocessing.MinMaxScaler.html) para normalizar.
 - En el caso de y_train e y_test. Cogemos el valor  máximo de y_train y dividimos todos los valores de ambos arrays por este valor.

#### Normalización de las imágenes. Train_array_images y Test_array_images.
 - En este caso tenemos dos numpy arrays que representan todas las  imágenes de train y de test en el formato adecuado.
 
 - Para normalizar dichos valores, vamos a dividir todos ellos entre 255. Ya que esté es el  máximo valor que tenemos en el formato RGB que le hemos dado a todas las imágenes.


### Implementación del modelo de dato numéricos y categóricos.

Como he dicho anteriormente, vamos a trabajar con Keras para crear nuestro modelo.

La partes de una red neuronal  son:
	- Los valores de activación "w". Representan el  peso de cada dato.
	- Los bias "b".
	- Forward propagation. Calcula la función de cost.
	- Backward propagation. Emplea el valor de "cost" calculado en el "Forward" para optimizar los valores de activación y los bias.

Keras nos permite implementaruna red neuronal indicando que función de activación queremos, número de capas que queremos y cuantas neuronas queremos en cada capa.
Y finalmente indicando que tipo de optimización queremos. Normalmente se emplea la optimización "Adam".

```
def create_mlp(dim, regress=False):
    
    # define our MLP network
    model = Sequential()
    model.add(Dense(32, input_dim=dim, activation="relu"))
    model.add(Dense(16, input_dim=dim, activation="relu"))
    model.add(Dense(8, input_dim=dim, activation="relu"))
    model.add(Dense(4, activation="relu"))
    
    # check to see if the regression node should be added
    if regress:
        model.add(Dense(1, activation="linear"))
    
    # return our model
    return model
```
Como parámetros recibe las dimensiones del X_train.

Vemos que se trata de una red neuronal formada por 5 capas que emplea la función de activación "relu" y posteriormente se emplea el optimizador "Adam" aunque no se vea en la función.

Como vemos indicamos ue la primera capa tiene 32 neuronas, la siguiente 16 ... y así hasta que llegamos a la última capa. 

La última capa tiene una sola neurona porque estamos buscando un valor en concreto, el **precio** de la vivienda. Y la acivación en ese caso es "linear".

### Implementación del modelo de dato numéricos y categóricos.

A la hora de crear un modelo para imágenes con Keras, introducimos nuevos steps en el proceso.
En este caso vamos a emplear un modelo convulocional que va contar con una capas Convulociones, funciones de activación, BatchNormalization, MxPooling y Dropout.

```
def create_cnn(width, height, depth, filters=(16, 32, 64), regress=False):
    # TensorFlow/channels-last ordering
    inputShape = (height, width, depth)
    chanDim = -1
    # define the model input
    inputs = Input(shape=inputShape)
    # loop over the number of filters
    for (i, f) in enumerate(filters):
        # if this is the first CONV layer then set the input
        # appropriately
        if i == 0:
            x = inputs
        # CONV => RELU => BN => POOL
        x = Conv2D(f, (3, 3), padding="same")(x)
        x = Activation("relu")(x)
        x = BatchNormalization(axis=chanDim)(x)
        x = MaxPooling2D(pool_size=(2, 2))(x)
        
        x = Flatten()(x)
    x = Dense(16)(x)
    x = Activation("relu")(x)
    x = BatchNormalization(axis=chanDim)(x)
    x = Dropout(0.5)(x)
    
    # apply another FC layer, this one to match the number of nodes
    # coming out of the MLP
    x = Dense(4)(x)
    x = Activation("relu")(x)
    
    # check to see if the regression node should be added
    if regress:
        x = Dense(1, activation="linear")(x)
        
    # construct the CNN
    model = Model(inputs, x)
    
    # return the CNN
    return model
```
L a función recibe como parámetros las dimensiones de nuestras imágenes.  En este caso son (64,64,3) .

Esas dimensiones se equipara al input y de forma automática se le da un valor al batch.

A partir de aquí vemos que se emplean 3 filtros. Y que el proceso de convolución, activación, Batch Normalization y dropout se aplica a cada uno de esos filtros.

Podemos cambiar el número de filtros que aplicamos, el tipo de función de activación que queremos emplear, el valor del dropout y el tipo de optimización en búsqueda de mejores resultados.

### Combinación de ambos modelos.

Finalmente combinamos ambos modelos para crear un único modelo.
Empleamos el siguiente código para hacerlo;

```
mlp = create_mlp(X_train.shape[1], regress=True)
cnn = create_cnn(64, 64, 3, regress=True)

# create the input to our final set of layers as the *output* of both
# the MLP and CNN
combinedInput = concatenate([mlp.output, cnn.output])

# our final FC layer head will have two dense layers, the final one
# being our regression head
x = Dense(1, activation="relu")(combinedInput)
x = Dense(1, activation="linear")(x)

# our final model will accept categorical/numerical data on the MLP
# input and images on the CNN input, outputting a single value (the
# predicted price of the house)
model = Model(inputs=[mlp.input, cnn.input], outputs=x)
```

Una vez tenemos un único modelo, empleamos nuestros datos e imágenes para entrenarlo:
```
# compile the model using mean absolute percentage error as our loss,
# implying that we seek to minimize the absolute percentage difference
# between our price *predictions* and the *actual prices*
opt = Adam(lr=1e-3, decay=1e-3 / 200)
model.compile(loss="mean_absolute_percentage_error", optimizer=opt)
# train the model
print("[INFO] training model...")
model.fit(
    x=[X_train, array_train_images], y=y_train,
    validation_data=([X_test, array_test_images], y_test),
    epochs=200, batch_size=8)
# make predictions on the testing data
print("[INFO] predicting house prices...")
preds = model.predict([X_test, array_test_images])
```

### Obtención de resultados.

Ahora ya tenemos nuestro modelo entrenado y podemos utilizarlo para predecir el valor del precio de una vivienda, que era el objetivo final de este proyecto.

Empleamos el siguiente código para hacerlo:

```
import locale

# make predictions on the testing data
print("[INFO] predicting house prices...")
preds = model.predict([X_test,array_test_images])

# compute the difference between the *predicted* house prices and the
# *actual* house prices, then compute the percentage difference and
# the absolute percentage difference
diff = preds.flatten() - y_test
percentDiff = (diff / y_test) * 100
absPercentDiff = np.abs(percentDiff)

# compute the mean and standard deviation of the absolute percentage
# difference
mean = np.mean(absPercentDiff)
std = np.std(absPercentDiff)

# finally, show some statistics on our model
locale.setlocale(locale.LC_ALL, "en_US.UTF-8")
df = pd.concat([df,df_test])
print("[INFO] avg. house price: {}, std house price: {}".format(
    locale.currency(df["Price"].mean(), grouping=True),
    locale.currency(df["Price"].std(), grouping=True)))
print("[INFO] mean: {:.2f}%, std: {:.2f}%".format(mean, std))
```

Este código nos devolverá dos valores:
 - Mean: Hace referencia al porcentaje de veces que el modelo no acierta el precio. Por lo tanto tenemos  qe disminuir este valor todo lo que podamos.
 
 - Sdt: Este valor representa la diferencia de error cada vez que no acertamos el precio. Por lo tanto también deberíamos disminuirlo hasta el mínimo.

Para disminuir dichos valores tenemos que hacer pruebas y cambiar los hiperparámetros que utilizamos en la creación de los modelos en búsqueda del mejor resultado.
